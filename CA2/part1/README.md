## CA2 - Parte 1

---
### O que é um build?

Em desenvolvimento de software, <span style="color:limegreen">**build**</span> é o termo usado para identificar uma versão compilada de um programa. Ou seja, quando as linhas de código escritas em linguagem de alto nível são traduzidas para linguagem de máquina, que um computador é capaz de entender.

<span style="color:salmon">
Um build, automatiza todos os passos usados na implantação de um sistema, poupando tempo e trabalho. Além de manter um padrão de rotina e minimizando a ocorrência de erros.
</span>

### Quais as características que um build deve ter?


Um software deve ser:


- [x] `Completo` - deve ser capaz de satisfazer necessidades explícitas e implícitas, os programadores devem-se preocupar em atender aos requisitos do cliente, mas também criar soluções para possíveis situações e dar a elas;

- [x] `Repetível` - várias execuções devem sempre produzir o mesmo resultado (isolar bugs e permitir a automação);

- [x] `Agendável` - significa que todas as funcionalidades escolhidas serão instaladas automaticamente e em sequência, sem erros;

- [x] `Portável` - A portabilidade garante que o sistema poderá ser executado ou compilado em diferentes arquiteturas.


### Gerenciador de build

Existem algumas ferramentas que facilitam a constituição de um build, que podem ser chamadas de gerenciadores de build.
Os gerenciadores de build possuem mecanismos que facilitam a a constituição de um build. Existem várias ferramentas para esse fim. 

**Infra seguem alguns exemplos:** 

- ![#c5f015](https://via.placeholder.com/15/00CC00/000000?text=+) **Ferramentas de automação GNU:**
 make, automake, autoconf, autoheader, libtool, gettext, gcc e pkg-config;

- ![#c5f015](https://via.placeholder.com/15/00CC00/000000?text=+) **Ferramentas de automação alternativas:**
pymake, Cmake, Cons, SCons, qmake, makeapp, JAM e waf;

- ![#1589F0](https://via.placeholder.com/15/00CC00/000000?text=+) **Baseadas em Java:**
Ant, Maven, Gant e Gradle;

- ![#c5f015](https://via.placeholder.com/15/00CC00/000000?text=+) **.NET:**
MsBuild, Nant, Byldan,e NMaven;


### Gradle - _gerenciador baseado em Java_


###### Mas então, o que é o Gradle?

O **Gradle** é um sistema avançado de automatização de builds que une o melhor da flexibilidade do **Ant** com o gerenciamento de dependências e as convenções do **Maven**. 

Os arquivos de build são scripts escritos na linguagem **Groovy**, ao contrário dos formatos de construção do Ant e Maven que usam arquivos **XML**. Por serem baseados em scripts, os arquivos do Gradle permitem que sejam realizadas tarefas de programação no seu arquivo de configuração. O Gradle tem ainda um sistema de plugins que permite adicionar novas funcionalidades extra ao core.

###### Conceitos básicos 

No Gradle pode-se dizer que se resume a **dois conceitos básicos**:

| Projetos _(Projects)_                                                                                                                                                                                               | Tarefas _(Tasks)_                                                                                                                                                                                                                                                     |
|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Todo o build da ferramenta é feita a partir de um ou mais projetos. A representação do projeto dependerá de como vai ser utilizado o **Gradle**. Um projeto pode representar um JAR ou até mesmo uma aplicação web. | Cada projeto é constituido por uma ou mais tasks. Cada uma significa um pedaço de trabalho que uma build vai executar. É possível ter tasks que fazem a compilação de classes, criação de JARs e até mesmo a publicação de arquivos para um repositório específico. |

###### Interface da linha de comandos no windows:



* **Para executar uma tarefa:** `./gradlew [taskName...] `

* **Para ver uma lista de todas as tarefas de compilação disponíveis para o projeto:** `./gradlew tasks `

* **Para ver uma lista de todas as tarefas incluindo as tarefas em cache, etc:** ` ./gradlew tasks --all`

* **Para executar tarefa de testes:** ` ./gradlew test`

* **compila o código do projeto:** ` ./gradlew build`

* **Compila o código do projeto e imprime o registo:** ` ./gradlew build --info`

* **Inicializa um projeto no diretório corrente:** ` ./gradlew  init`

* **Excluir pasta de compilação:** ` ./gradlew  clean`
* **Spring boot Gradle contém a tarefa bootRun que nos ajuda a compilar e executar o aplicativo spring boot:** ` ./gradlew  bootRun`

---

### Parte prática:

###### -> Criar: Task para executar o servidor, teste, task copy, task zip



<span style="color:blue">
1.</span>

Copiar para a pasta _**CA2 part1**_ o exemplo da aplicação a utilizar, disponível no link abaixo:
   https://bitbucket.org/luisnogueira/gradle_basic_demo/src/master/


<span style="color:blue">
2.</span>

Seguir instruções conforme doc _readme_ que consta no exemplo. 

<span style="color:orange">
_CA2/part1/gradle_basic_demo/README.md_
</span>

Testar a aplicação de _chat_;


<span style="color:blue">
3.</span>

Adicionar uma task para executar o servidor (**runServer**):

<span style="color:orange">
_CA2/part1/gradle_basic_demo/build.gradle_
</span>

    task runServer(type:JavaExec, dependsOn: classes){
    group = "DevOps"
    description = "Launches the server "
    classpath = sourceSets.main.runtimeClasspath

    mainClass = 'basic_demo.ChatServerApp'
    args '59001'
    }

<span style="color:blue">
4.</span>

**Adicionar um teste unitário:** 

<span style="color:blue">
4.1</span>

Criar **pasta de teste** e adicionar o código conforme mencionado infra:
<span style="color:orange">_CA2/part1/gradle_basic_demo/src/test/java/basic_demo/AppTest.java_</span>

![Folder](Criação%20pasta%20teste.png)


    /*
     * This Java source file was generated by the Gradle ’init’ task.
     */

    package basic_demo;
    import org.junit.Test;
    import static org.junit.Assert.*;

      public class AppTest {
      @Test public void testAppHasAGreeting() {
      App classUnderTest = new App();
      assertNotNull("app should have a greeting", classUnderTest.getGreeting());
      }
      }

<span style="color:blue">
4.2</span>

Para que seja possível executar os testes é necessário adicionar a dependência de **junit**:
      
      testImplementation 'junit:junit:4.12'

![Folder](Add dependency.png)

<span style="color:blue">
5.</span>

Criar uma **task Copy** - da pasta **src** para uma nova pasta **(build/backup_test/src_backup)** que será utilizada como backup:

  
    task copy(type: Copy){
    from "src/"
    into "build/backup_test/src_backup"
    }

<span style="color:blue">
6.</span>

Criar uma nova **task Zip** - da pasta **src** para uma nova pasta **(build)** que será utilizada como arquivo:


      task zip(type: Zip){
      from "src/"
      archiveName "src_backup.zip"
      destinationDir(file('build'))
      }

<span style="color:blue">
7.</span>

No final marcar o repositório com a tag:

<span style="color:mediumseagreen">
ca2-part1</span>.

      git add .
      git commit –m “Colocar texto de commit”
      git push origin master
      git tag ca2-part1
      git push origin --tags