package com.greglturnquist.payroll;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.aspectj.runtime.internal.Conversions.intValue;
import static org.junit.jupiter.api.Assertions.*;


public class EmployeeTest {


    @Test
    @DisplayName("testing the creation of Employees - success")
    public void createEmployeeWithSuccess() {
        //Arrange
        String firstName = "Cristiana";
        String lastName = "Carvalho";
        String description = "Make unit tests";
        String jobTitle = "Tester";
        LocalDate entryDate = LocalDate.of(2021, 1, 1);
        int jobYears = LocalDate.now().getYear() - entryDate.getYear();
        String email = "ccc@isep.ipp.pt";

        //Act
        Employee employee1 = new Employee(firstName, lastName, description, jobTitle, entryDate, email);

        //Assert
        assertNotNull(employee1);
        assertEquals(firstName, employee1.getFirstName());
        assertEquals(lastName, employee1.getLastName());
        assertEquals(description, employee1.getDescription());
        assertEquals(jobTitle, employee1.getJobTitle());
        assertEquals(jobYears, employee1.getJobYears());
        assertEquals(email, employee1.getEmail());
    }

    @Test
    @DisplayName("testing the creation of Employees - Empty/null fields (FirstName")
    public void createEmployeeFirstNameEmptyOrNull() {
        //Arrange
        String firstName = "";
        String firstName2 = null;
        String lastName = "Carvalho";
        String description = "Make unit tests";
        String jobTitle = "Tester";
        LocalDate entryDate = LocalDate.of(2021, 1, 1);
        String email = "ccc@isep.ipp.pt";

        //Act
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            new Employee(firstName, lastName, description, jobTitle, entryDate, email);
            new Employee(firstName2, lastName, description, jobTitle, entryDate, email);
        });

        //Assert
        assertTrue(exception.getMessage().contains("First name cannot be blank."));
    }

    @Test
    @DisplayName("testing the creation of Employees - Empty/null fields (LastName")
    public void createEmployeeLastNameEmptyOrNull() {
        //Arrange
        String firstName = "Cristiana";
        String lastName = "";
        String lastName2 = null;
        String description = "Make unit tests";
        String jobTitle = "Tester";
        LocalDate entryDate = LocalDate.of(2021, 1, 1);
        String email = "ccc@isep.ipp.pt";

        //Act
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            new Employee(firstName, lastName, description, jobTitle, entryDate, email);
            new Employee(firstName, lastName2, description, jobTitle, entryDate, email);
        });

        //Assert
        assertTrue(exception.getMessage().contains("Last name cannot be blank."));
    }

    @Test
    @DisplayName("testing the creation of Employees - Empty/null fields (Description")
    public void createEmployeeDescriptionEmptyOrNull() {
        //Arrange
        String firstName = "Cristiana";
        String lastName = "Carvalho";
        String description = "";
        String description2 = null;
        String jobTitle = "Tester";
        LocalDate entryDate = LocalDate.of(2021, 1, 1);
        String email = "ccc@isep.ipp.pt";

        //Act
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            new Employee(firstName, lastName, description, jobTitle, entryDate, email);
            new Employee(firstName, lastName, description2, jobTitle, entryDate, email);
        });

        //Assert
        assertTrue(exception.getMessage().contains("Description cannot be blank."));
    }

    @Test
    @DisplayName("testing the creation of Employees - Empty/null fields (JobTitle")
    public void createEmployeeJobTitleEmptyOrNull() {
        //Arrange
        String firstName = "Cristiana";
        String lastName = "Carvalho";
        String description = "Make unit tests";
        String jobTitle = "";
        String jobTitle2 = null;
        LocalDate entryDate = LocalDate.of(2021, 1, 1);
        String email = "ccc@isep.ipp.pt";

        //Act
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            new Employee(firstName, lastName, description, jobTitle, entryDate, email);
            new Employee(firstName, lastName, description, jobTitle2, entryDate, email);
        });

        //Assert
        assertTrue(exception.getMessage().contains("Job title cannot be blank."));
    }

    @Test
    @DisplayName("calculate years in company - Success")
    public void calculateYearsInCompanyWithSuccess() {
        //Arrange
        String firstName = "Cristiana";
        String lastName = "Carvalho";
        String description = "Make unit tests";
        String jobTitle = "Tester";
        String email = "ccc@isep.ipp.pt";

        LocalDate entryDate = LocalDate.of(2021, 3, 20);
        int jobYears = intValue(java.time.temporal.ChronoUnit.YEARS.between(entryDate, LocalDate.now()));

        LocalDate entryDate2 = LocalDate.of(2021, 12, 20); //Ainda não completou um ano
        int jobYears2 = intValue(java.time.temporal.ChronoUnit.YEARS.between(entryDate2, LocalDate.now()));

        //Act
        Employee employee1 = new Employee(firstName, lastName, description, jobTitle, entryDate, email);
        Employee employee2 = new Employee(firstName, lastName, description, jobTitle, entryDate2, email);

        //Assert
        assertEquals(1, jobYears);
        assertEquals(1, employee1.getJobYears());
        assertEquals(0, jobYears2);
        assertEquals(0, employee2.getJobYears());
    }

    @Test
    @DisplayName("calculate years in company - Fail because entry date is in future")
    public void calculateYearsInCompanyFailWrongDate() {
        //Arrange
        String firstName = "Cristiana";
        String lastName = "Carvalho";
        String description = "Make unit tests";
        String jobTitle = "Tester";
        String email = "ccc@isep.ipp.pt";

        //Act
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            LocalDate entryDate = LocalDate.of(2025, 3, 20);
            new Employee(firstName, lastName, description, jobTitle, entryDate, email);
        });

        //Assert
        assertTrue(exception.getMessage().contains("The entry date cannot be later than the current date"));
    }

    @Test
    @DisplayName("testing the creation of Employees - Empty/null fields (Email)")
    public void createEmployeeEmailEmptyOrNull() {
        //Arrange
        String firstName = "Cristiana";
        String lastName = "Carvalho";
        String description = "Make unit tests";
        String jobTitle = "Tester";
        LocalDate entryDate = LocalDate.of(2020, 3, 20);

        //Act
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            String email = "";
            new Employee(firstName, lastName, description, jobTitle, entryDate, email);
        });

        //Assert
        assertTrue(exception.getMessage().contains("Email cannot be blank."));
    }

    @Test
    @DisplayName("testing the creation of Employees - Empty/null fields (Email)")
    public void createEmployeeEmailwithoutsign() {
        //Arrange
        String firstName = "Cristiana";
        String lastName = "Carvalho";
        String description = "Make unit tests";
        String jobTitle = "Tester";
        LocalDate entryDate = LocalDate.of(2020, 3, 20);

        //Act
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            String email = "zzzzzz";
            new Employee(firstName, lastName, description, jobTitle, entryDate, email);
        });

        //Assert
        assertTrue(exception.getMessage().contains("The email must have at sign."));
    }
}

