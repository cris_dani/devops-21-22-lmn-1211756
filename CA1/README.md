## GIT -  sistema de controlo de versão

---
### Instalar o GIT

O **GIT** é um dos mais famosos sistemas de controlo de versões de código sendo totalmente gratuito e, pode ser instalado em Mac, Linux, Windows a partir do site oficial.

###### Executar os seguintes passos para instalar o GIT no Windows:
1. Aceder ao site oficial e fazer o download do instalador do GIT para Windows;
2. Depois de baixado, iniciar o assistente de instalação. Basta seguir as instruções na tela e, no final clicar em _Finish_ para concluir com  êxito a instalação;
3. Quando a instalação estiver concluida é necessário definir o **nome de utilizador**, **endereço de e-mail** e o **editor de texto**. Este passo é importante porque todos os commits no Git utilizam essas informações, e está imutavelmente anexado nos commits que vão ser realizados;
5. Abrir **powerShell** (ou outra linha de comandos) e digitar os comandos mencionados infra:


    git config -l   user.name "Cristiana Carvalho"
    git config -l   user.email "exemplo@seuemail.com.pt"
    it config --global  core.editor 'C:/Program Files/Notepad++/notepad++.exe' -multiInst -nosession

---


### Clone do repositório e criação de pasta CA1

Agora que o **GIT** está instalado e configurado no dispositivo, é necessário fazer o clone do repositório criado(**_repC_**) e o clone da aplicação (**_repA_**).
1. Aceder ao **Bitbucket** e criar um repositório novo;
2. Clonar os repositórios usando os comandos abaixo mencionados:


    repC - git clone https://Cris_Dani@bitbucket.org/cris_dani/devops-21-22-lmn-1211756.git
    repA - git clone https://github.com/spring-guides/tut-react-and-spring

3. Copiar a informação do tut react para dentro do repositório criado - **ter em atenção para não copiar o .git**.
4. Aceder ao projeto e criar pasta CA1;


    cd .\devops-21-22-lmn-1211756\
    mkdir CA1

5. Copiar toda a informação da pasta de **tut** para dentro da pasta **CA1** (criada no step anterior);


    cp -r .\tut-react-and-spring-data-rest\ CA1

---

###Os comandos Add e Commit - _sem branch_

######Sem ramificação

* Alterações ou adições de arquivos propostas são adicionadas ao índice (**staging area**) usando o comando **add**. Para adicionar qualquer arquivo, o comando é:


    git add .

* Para validar se a informação foi enviada para a **staging area** conforme pretendido usar o comando:


    git status

* Para confirmar estas mudanças, então deve ser usado o comando **commit**. O arquivo é enviado para o **HEAD**, mas ainda não para o repositório remoto.


    git commit –m “Adicionar qualquer mensagem sobre o commit aqui”


* Para enviar estas alterações locais para o repositório remoto, executar:


    git push origin master


* Para visualizar o histórico de alterações e ter acesso ao **hash do commit** - (identificação única de cada commit) bem como a tag;


    git log

* É recomendado criar rótulos para _releases de software_, caso algo se perca ou fique com erros e seja necessário voltar atrás. Usar o **tag** para demarcar um ponto (**commit**).
  (_esta corresponde à tag inicial do projeto_). Para enviar para o repositório usar o segundo comando;


    git tag <nome da tag>
    git push origin --tags

---

## Desenvolver uma nova funcionalidade - campo JobYears

Será necessário acrescentar um campo onde seja possível obter informação da antiguidade do colaborador na empresa (**jobYears**).

######Critérios de aceitação:

1. Ter métodos _get_ e _set_ para aceder ao referido campo;
2. Os atributos não podem estar a nulo ou vazios;
3. O novo campo criado (**jobYears**), apenas deve permitir receber valores _integer_;

######Interpretações:

1. Para conseguir calcular a antiguidade do colaborador na empresa será necessário criar uma coluna que registe a informação da data de entrada do colaborador na empresa (**entryDate**);
2. A antiguidade será calculada usando essa data e a data de sistema (**java.time.temporal.ChronoUnit.YEARS**);
3. Apesar de termos acrescentado a informação de data de entrada (**entryDate**), esta data não será exibida na aplicação, serve apenas de apoio para o cálculo da coluna a ser exibida (**jobYears**);

##Procedimento para criação e gestão de um novo campo (_jobYears_)

Abrir **intellIJ** e seguir os steps infra:

######Pasta basic - src  -main - js - api - app.js

1. _Na tag::employee-list[]_ acrescentar informação de **jobYears**;

   _Na tag::employee[]_ igualmente acrescentar a informação de **jobYears**;


######Pasta basic - src - main - java - Employee

Acrescentar os dados infra:
1. Atributo **entryDate**;
2. Atributo **jobYears**;
3. No construtor, introduzir como parâmetro, a data de entrada do empregado na empresa (**entryDate**);
4. **JobYears** no Override equals, hashCode e toString;
5. Criar método de **getJobYears** e **setJobYears**;
6. Adicionar, no construtor, o cálculo de antiguidade do empregado, diferença entre a data atual e a data de registo em sistema (**java.time.temporal.ChronoUnit.YEARS**), apresentar o resultado em integer (**intValue**);
7. Criar método de validação dos campos (**isValidEmployee**) - (_não podem ser introduzidos dados a vazio ou nulos_), uma vez que se tratam de strings usar o **trim().isEmpty()**, caso algum dos campos não cumpra os requisitos vai retornar uma exceção (**IllegalArgumentException**) com informação de qual o campo que não cumpre os requisitos;
   Para a data de entrada do empregado (**entryDate**),não permitir que seja colocada uma data posterior à data atual;
8. Colocar esse método de validação (**isValidEmployee**) no construtor (local onde será criado o empregado), para impedir a criação do mesmo com os campos incorretos;


######Pasta basic - src - main - java - DatabaseLoader
1. Acrescentar dados referentes à antiguidade do trabalhador;


######Pasta Test

1. Para criação do **empregado**, testar caso de sucesso, com todos os campos corretamente preenchidos, e testar insucesso falhando o preenchimento dos campos infra:

* **firstName** - vazio ou nulo;

* **lastName** - vazio ou nulo;

* **description** - vazio ou nulo;

* **jobTitle** - vazio ou nulo;

* **entryDate** - data posterior à data atual (sistema);

2. Para cálculo da **antiguidade do colaborador**, testar caso de sucesso e, caso de insucesso:

* **entryDate** - insucesso, quando a data de entrada do colaborador está no futuro;

## Testar funcionalidade no localhost

1. Aceder pasta onde se encontra o programa usando os comandos **cd** seguido da localização (pastas);
2. Arrancar a aplicação com o comando infra:


    ./mvnw spring-boot:run

3. Abrir **browser** e digitar: http://localhost:8080/ e validar se, os campos criados na aplicação estão visiveis;



## Enviar nova funcionalidade para o repositório

Seguir os passos indicados acima com nome **_Os comandos Add e Commit - sem branch**_. Encontram-se também resumidos infra:


    cd .\devops-21-22-lmn-1211756\
    cd .\CA1\
    cd .\tut-react-and-spring-data-rest\
    cd basic
    git status
    git add .
    git commit –m “Adicionar nova funcionalidade - JobYears”
    git push origin master
    git tag v1.2.0
    git push origin --tags


## Criar um _branch_

Criar um **branch** (_ramificação_) significa criar uma linha independente de desenvolvimento.

**Branches** ("ramos") são utilizados para desenvolver funcionalidades isoladas umas das outras. O branch master é o branch "padrão" quando é criado um repositório. É possível ultilizar outros branches para desenvolver e no final fazer **merge** com o master.

1. Na linha de comandos navegar até ao repositório;
2. Criar um **branch** com o nome de **_email-field_** usando o comando infra:


    git branch <nome do branch> (neste caso- email-field)

3. Depois de criar o referido **branch** é necessário definir em que **branch** vou trabalhar, o apontador (**HEAD**) terá de estar direcionado para este referido ramo;


    git checkout email-field


###Os comandos Add e Commit - Com branch

######Ramificando


* Listar todas as ramificações do repositório:


    git branch


* Para criar uma ramificação usar:


    git branch <nome do branch>


* Para mudar o meu **HEAD** (apontador para o branch a trabalhar). No novo branch de desenvolvimento, é possível alterar o código sem ter que mudar nada na versão principal;


    git checkout <nome do branch>

* Um branch não está disponível a outros a menos que seja enviado para o repositório remoto;


    git push origin <nome do branch>


* Após realização de todas as tarefas propostas é necessário voltar para o master para que possa ser feito o merge;


    git checkout master
    git merge <nome do branch>




## Desenvolver uma nova funcionalidade - campo Email

Será necessário acrescentar um campo onde seja possível guardar a informação referente ao email do colaborador na empresa (**email**).

######Critérios de aceitação:

1. Ter métodos _get_ e _set_ para aceder ao referido campo;
2. Os atributos não podem estar a nulo ou vazios;
3. email deve pelo menos conter o @;


##Procedimento para criação e gestão de um novo campo (_email_)

Abrir **intellIJ** e seguir os steps infra:

######Pasta basic - src  -main - js - api - app.js

1. _Na tag::employee-list[]_ acrescentar informação de **email**;

   _Na tag::employee[]_ igualmente acrescentar a informação de **email**;


######Pasta basic - src - main - java - Employee

Acrescentar os dados infra:
1. Atributo **email**;
2. No construtor, introduzir como parâmetro, o email do empregado (**email**);
3. **email** no Override equals, hashCode e toString;
4. Criar método de **getEmail** e **setEmail**;
5. Ao método já criado anteriomente para validação dos campos, (**isValidEmployee**) acrescentar a validação do campo email - (_não podem ser introduzidos dados a vazio ou nulos_), uma vez que se tratam de strings usar o **trim().isEmpty()**, caso algum dos campos não cumpra os requisitos vai retornar uma exceção (**IllegalArgumentException**) com informação de qual o campo que não cumpre os requisitos;
   deve pelo menos conter o @;
6. Colocar esse método de validação (**isValidEmployee**) no construtor (local onde será criado o empregado), para impedir a criação do mesmo com os campos incorretos;


######Pasta basic - src - main - java - DatabaseLoader
1. Acrescentar dados referentes ao email do trabalhador;


######Pasta Test

1. Para criação do **empregado**, nomeadamente o campo de **email**, testar caso de sucesso, com o campo corretamente preenchido, e testar insucesso falhando o preenchimento do campo:

* **email** - vazio ou nulo;
*  **email** - não colocar o @;


## Testar funcionalidade no localhost

1. Aceder pasta onde se encontra o programa usando os comandos cd seguido da localização (pastas);
2. Arrancar a aplicação com o comando infra:


    ./mvnw spring-boot:run

3. Abrir **browser** e digitar: http://localhost:8080/ e validar se, os campos criados na aplicação estão visiveis;

## Enviar nova funcionalidade para o repositório

Seguir os passos indicados acima com nome **_Os comandos Add e Commit - com branch**_. Encontram-se resumidamente infra:


    cd .\devops-21-22-lmn-1211756\
    cd .\CA1\
    cd .\tut-react-and-spring-data-rest\
    cd basic
    git status
    git add .
    git commit –m “Adicionar nova funcionalidade - Email”
    git push --set-upstream origin email-field
    git checkout master
    git merge email-field
    git push
    git tag v1.3.0
    git push origin --tags


## Correção de um bug

1. Criar um branch com nome **fix-invalid-email**:


    git branch fix-invalid-email

2. Aceder ao **branch** para resolução de um bug (garantir que o email tem pelo menos o @);


    git checkout fix-invalid-email

3. Garantir cobertura de testes para a validação acrescentada;

## Enviar nova funcionalidade para o repositório

    cd .\devops-21-22-lmn-1211756\
    cd .\CA1\
    cd .\tut-react-and-spring-data-rest\
    cd basic
    git status
    git add .
    git commit –m “o email deve conter o "@"”
    git push --set-upstream origin fix-invalid-email
    git checkout master
    git merge fix-invalid-email
    git push
    git tag v1.3.1
    git push origin --tags


## Alternativas ao GIT

O **Git** tem várias qualidades: é popular, open source, é rápido (até um certo tamanho de projeto) e tem um bom design interno. Porém, simplicidade não está entre essas qualidades. Pelo contrário, o **Git** é bastante complexo e sua interface é pouco intuitiva.
Existem vários locais onde os arquivos podem-se encontrar:
- _stash_;
- diretório de trabalho (_working tree_);
- _index_ (também chamado de stage area ou cache);
- repositório local e remoto;

A parte do _index_ é controlado automaticamente pela ferramenta de **Mercurial** ou **Subversion** enquanto que, no **Git** essa gestão tem de ser feita pelo utilizador.

Por outro lado, a complexidade de comandos pode influenciar a utilização da ferramenta.

######Subversion

O Subversion é simples e direto. Para usar o **Subversion**, apenas é necessário entender os conceitos básicos de controlo de versão: arquivo, diretório de trabalho, revisão, changeset, ramos e etiquetas (tags).
É também necessário conhecer critérios de implementação do **Subversion** em relação à ramificação através de diretórios, e à convenção da estrutura de diretórios trunk/branches/tags.

Há apenas dois lugares onde o fontes podem estar:
- diretório de trabalho;
- repositório;


######Mercurial

Para além dos conceitos de controle de versão centralizado, também é necessário entender conceitos básicos de sincronização de repositórios.

O **Mercurial** não possui nenhuma particularidade de implementação que fique exposta na estrutura do projeto ou nos comandos.


**_Branch no Mercurial_**

Apesar do conceito entre branchs do **Git** e **Mercurial** serem similares, há um ponto importante no **Mercurial**: Os branchs são permanentes. Isso significa que os branchs não podem ser removidos como no **Git**. Após ser criado merge entre branchs, a que recebeu o merge permanece enquanto a outra recebe apenas o status de desativada.



**O **Subversion** deve ser a primeira alternativa por ser mais simples. Mas, se houver necessidade de começar com o controlo de versão distribuído, considerar o **Mercurial** porque é apenas um pouco mais complexo que o **Subversion**. O **Git**, apesar da popularidade, não é recomendado devido à sua alta complexidade.**

######Subversion - instalação e principais comandos

* sudo apt-get install subversion;
* svn help - irá apresentar a lista de comandos disponíveis;
* svn import - para copiar uma árvore de arquivos não versionada num repositório;
* svn checkout - para criar cópia local do projeto que está a ser trabalhado;
* svn update - para sincronizar a cópia de trabalho com o repositório remoto. Executando o comando é possível verificar quais os comandos adicionados e não constavam na cópia de trabalho;
* svn add - adicionar novo arquivo no repositório;
* svn mkdir - para criar pasta no repositório;
* svn status - para saber quais as alterações de diretórios realizadas;
* svn commit -m "frase resumo" - efetivar todas as alterações realizadas para o servidor;


