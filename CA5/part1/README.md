## CA5 - parte 1

---

###### CI/CD Pipelines with Jenkins

---

#### Sobre o Jenkins

![](Images/img_10.png)

O Jenkins, assim como outras ferramentas de **CI (Continuous Integration)**, é utilizado na automação de builds durante o desenvolvimento do software, prevenindo e alertando qualquer problema de integração após um commit ou merge de código, podendo ainda disparar de forma automática testes unitários e funcionais automatizados da aplicação, para garantir que nada saiu errado. Numa equipa, são comuns constantes integrações de código durante o dia, tornando ainda mais necessário o uso destas ferramentas. 

É a ferramenta de CI mais popular e, por consequência, existem diversos plugins e recursos disponíveis. 

**O Jenkins oferece:**

-[x] Integração com ferramentas de controle de versão como SVN, Git, Mercurial;
-[x] Build e deploy do projeto;
-[x] Suporte a Maven e Ant;

O Jenkins pode ser baixado em .war e executável. Para executar .war basta fazer o deploy em umservidor como Tomcat, Jetty, Jboss etc.

**Vantagens:**
* Builds periódicos;
* Credencias;
* Agente SSH;
* Testes automatizados;
* Possibilita analise de código;
* Identificar erros mais cedo;
* Fácil de operar e configurar;
* Builds em diversos ambientes;


___ 

#### Parte prática CA5-part1

<span style="color:blue">
1.</span> 

Instalar o **jenkins.war** :

    https://www.jenkins.io/download/

![](Images/img.png)

<span style="color:blue">
2.</span>

Na linha de comandos digitar:

    java -jar jenkins.war


<span style="color:blue">
2.</span>

Copiar o CA2 part1 (gralde_basic_demo) para o CA5 part 1.

![](Images/img_1.png)

<span style="color:blue">
2.</span>

Criar o ficheiro de configuração **jenkinsfile** com as configurações necessárias:

![](Images/img_2.png)

* Checkout;
* Assemble;
* Test;
* Archive;

<span style="color:blue">
3.</span>

Abrir página principal do jenkins e selecionar **Create a Job**:

![](Images/img_3.png)

<span style="color:blue">
4.</span>

Nomear o ficheiro: **CA5_part1** e selecionar **pipeline**:

![](Images/img_4.png)

<span style="color:blue">
5.</span>

**Seguir os steps infra:**

- [x] Selecionar Pipeline script from SCM;
- [x] Selecionar Git as SCM;
- [x]  URL para repositório: https://bitbucket.org/cris_dani/devops-21-22-lmn-1211756;
- [x] Especificar o branch: */master;
- [x] Especificar o path para o script: CA5/part1/gradle_basic_demo/Jenkinsfile;


![](Images/img_8.png)

<span style="color:blue">
6.</span>

Enviar informação para o repositório para que possa ser processado o ficheiro **Jenkinsfile**.

    git add .
    git commit -m "Create folders: CA5 part1 (resolve #32) Clone gradle_basic_demo CA2/part1 (resolve #33) Send the Jenkinsfile to the repository (resolve #31)"
    git push


<span style="color:blue">
7.</span>

**Abrir:**

    http://localhost:8080/ 

* Selecionar o CA5_part1;
* Escolher a opção build now;

![](Images/img_9.png)

<span style="color:blue">
8.</span>

Como estava a dar um erro no build "**Could not find GradleWrapperMain**" foi necessário fazer uns ajustes:

- Adicionar o wrapper no repositório para o jenkins usar **.\gradle\wrapper\gradle-wrapper.jar**;
- Actualizar o gitignore para deixar enviar o respectivo ficheiro;


    git add -f .\gradle\wrapper\gradle-wrapper.jar
    git commit .\gradle\wrapper\gradle-wrapper.jar .\.gitignore -m "change gitignore, add gradle-wrapper.jar"
    git push

<span style="color:blue">
9.</span>

_Fazer novamente o build no jenkins._

**Abrir:**

    http://localhost:8080/ 

* Selecionar o CA5_part1;
* Escolher a opção build now;



![](Images/img_7.png)

<span style="color:blue">
10.</span>

Enviar a informação para o repositório:

    git add .
    git commit -m "Edit readme"
    git push
    git tag ca5-part1
    git push origin ca5-part1

