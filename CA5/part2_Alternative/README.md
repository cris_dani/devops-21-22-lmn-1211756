## CA5 - parte 2 - Alternative Solution

---

###### CI/CD Pipelines with Jenkins

---

Existem diversas alternativas ao Jenkins. Infra seguem alguns exemplos:
* Bitbucket Pipelines - **O que escolhi implementar**
* Buddy
* FinalBuilder
* GoCD
* IBM Urbancode
* CircleCI
* TeamCity
* GitLab CI

## Jenkins

### Vantagens

- [x] **Opção de Hospedagem**: É mais um recurso importante do Jenkins, que pode ser instalado em qualquer sistema operacional como Windows, MacOS, Linux, etc. Você também pode executar o Jenkins na nuvem baixando e implantando em uma VM. Também se pode usar um contentor do Docker nele.
- [x] É o servidor de **código aberto** mais utilizado, portanto há muita ajuda disponivel;
- [x] A maior parte do trabalho de integração é automatizada. Como resultado, há menos problemas de integração. Isso também ajuda a economizar dinheiro e tempo durante o caminho do projeto.
- [x] Os problemas de codificação podem ser detectados assim que possível pelos engenheiros. Isso evita que eles tenham que lidar com integrações em grande escala e propensas a erros.
- [x] Suporta várias linguagens, por exemplo: Java, Python, etc.
- [x] integração com outras plataformas CI/CD: Jenkins suporta muitas plataformas CI/CD, não apenas o pipeline. Ele pode fazer a interação com outras ferramentas também. Nele estão disponíveis diversos plug-ins, que permitem aos usuários fazer conexões com outras plataformas de CI/CD.

### Desvantagens


- [x] Interface desatualizada;
- [x] Não é fácil de usar em comparação com as tendências atuais da interface do utilizador;
- [x] A sua configuração é complicada;
- [x] Plugins redundantes e menos atualizados;
- [x] Nem todos os  plugins são compatíveis com o pipeline declarativo;
- [x] Muita documentação desatualizada;


## Bitbucket Pipelines

### Vantagens

- [x] CI/CD integrada ao Bitbucket - A CI/CD em seu devido lugar, pertinho do seu código. Nenhum servidor para gerenciar, repositório para sincronizar ou gerenciamento de usuários para configurar.
- [x] Fácil instalação e configuração - Com o Pipelines, não há servidor de CI para instalar, gerenciamento de usuários para configurar ou repositórios para sincronizar. Basta ativá-lo com alguns cliques e estará pronto para usar.
- [x] Integração Jira com o melhor da categoria - Visibilidade contínua do backlog até a implementação: deixe a sua equipe saber exatamente o status de compilação com o Jira e quais problemas fazem parte de cada implementação com o Bitbucket.
- [x] Compatível com todas as plataformas - Permite criar testes com qualquer linguagem ou plataforma, como Java, JavaScript, PHP, Ruby, Python, .NET Code e outras.
- [x] Mapear a estrutura de ramificação - Os pipelines podem ser alinhados com a estrutura da ramificação, tornando mais fácil o trabalho com fluxos de trabalho ramificados, como ramificação de recursos ou fluxo de trabalho do Git.

### Desvatagens

- [x] Interface do utilizador e o fluxo de trabalho geral não são tão bons;
- [x] Ainda com poucos plugins disponiveis;
- [x] Os testes correm de forma mais lenta;
- [x] Bastante mais dificil de definir os steps;

###### Comece com Pipelines

O Bitbucket Pipelines é um serviço integrado de CI/CD integrado ao Bitbucket. Ele permite que seja criado, testado e até mesmo implantado automaticamente o código com base num arquivo de configuração no repositório. Essencialmente, são criados contentores na cloud. Dentro desses contentores, podem ser executados comandos (tal como se faz na máquina local), mas com todas as vantagens de um sistema novo, personalizado e configurado para as necessidades.

###### Entenda o arquivo YAML

Um pipeline é definido usando um arquivo **YAML** chamado **bitbucket-pipelines.yml**, localizado na raiz do repositório.




___


<span style="color:blue">
1.</span> 

Abrir bitbucket:

    https://bitbucket.org/cris_dani/devops-21-22-lmn-1211756/src/master/

Selecionar a opção piplines **(1)** seguida de build a gradle project **(2)**.

![](Images/img_1.png)



<span style="color:blue">
2.</span> 

Copiar o template de gradle project para a raiz do repositório:

![](Images/img_2.png)



<span style="color:blue">
3.</span> 

Para conseguir realizar o step de fazer o docker push para o docker hub é necessário definir username e password nas variáveis de repositório:

![](Images/img_3.png)


<span style="color:blue">
4.</span> 

O ficheiro deve conter a informação infra:

    #  Template Java Gradle build

    #  This template allows you to test and build your Java project with Gradle.
    #  The workflow allows running tests, code checkstyle and security scans on the default branch.

    # Prerequisites: appropriate project structure should exist in the repository.

    image: gradle:6.6.0

    pipelines:
    default:
    step:
    name: Check directory exist
    script:
     cd CA5/part2_Alternative/gradle_basic_demo

        - step:
            name: Gradle Clean and Assemble
            caches:
              - gradle
            script:
              - cd CA5/part2_Alternative/gradle_basic_demo
              - gradle clean assemble

        - step:
            name: Gradle Test
            caches:
              - gradle
            script:
              - cd CA5/part2_Alternative/gradle_basic_demo
              - gradle test

        - step:
            name: Gradle Jar
            caches:
              - gradle
            script:
              - cd CA5/part2_Alternative/gradle_basic_demo
              - gradle jar

        - step:
            name: Gradle Javadoc
            caches:
              - gradle
            script:
              - cd CA5/part2_Alternative/gradle_basic_demo
              - gradle javadoc

        - step:
            name: Build and Publish Docker Image
            services:
              - docker
            script:
              - cd CA5/part2_Alternative/gradle_basic_demo
              - gradle clean assemble
              - docker build -t 1211756/ca5_part2:alternative .
              - docker login --username $DOCKER_HUB_ID --password $DOCKER_HUB_PASSWORD
              - docker push 1211756/ca5_part2:alternative


<span style="color:blue">
5.</span> 

Correndo este scrip é esperado que o projeto faça build.

![](Images/img_4.png)

Imagem publicada no docker Hub:

    https://hub.docker.com/

![](Images/img_5.png)

<span style="color:blue">
6.</span> 

Enviar informação para o repositório:

    git add . 
    git commit -m "Alternative Solution - Bitbuket pipelines"
    git push


### Considerações

Tive bastante dificuldade em encontrar documentação que me ajudasse a implementar esta alternativa (**Bitbucket pipelines**), as tentativas de build foram sempre bastante mais demoradas do que no jenkins.
Alguma dificuldade também para definir username e password. É mais fácil manter a ultilização do **Jenkins**.
