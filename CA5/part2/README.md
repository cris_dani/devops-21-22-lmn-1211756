## CA5 - parte 2

---

###### CI/CD Pipelines with Jenkins

---



<span style="color:blue">
1.</span> 

Criar o ficheiro de configuração **jenkinsfile** no CA5-part2 com as configurações necessárias:


Será necessário gerar:
- [x] Javadoc;

![](Images/img_11.png)

- [x] Generate Jar;

![](Images/img_12.png)

- [x] Docker Image;

![](Images/img_13.png)

<span style="color:blue">
2.</span> 

Arrancar o jenkins.
Na linha de comandos digitar:

    java -jar jenkins.war

<span style="color:blue">
3.</span>

Será necessário instalar os plugins infra:

* **HTML Publisher** - para que seja possivel a publicação do javadoc;
* **Docker Pipeline** - para gerar a imagem e publicar no dockerHub;


[~] Abrir o localhost:

    http://localhost:8080/

No Dashboard **(1)** selecionar a opção _manage Jenkins_ **(2)** e depois _manage plugins_ **(3).**

![](Images/img_3.png)

No **Plugin Manager** selecionar a opção _available_ **(1)** e depois pesquisar por _html publisher_ **(2)** e por fim instalar **(3)**.

![](Images/img_4.png)

No **Plugin Manager** selecionar a opção _available_ **(1)** e depois pesquisar por _docker pipeline_ **(2)** e por fim instalar **(3)**.

![](Images/img_7.png)

<span style="color:blue">
4.</span>

Criar o ficheiro de configuração **Dockerfile** no CA5-part2 com as configurações necessárias:

    FROM ubuntu:18.04

    RUN apt-get update -y
    RUN apt-get install openjdk-11-jdk-headless -y

    COPY build/libs/basic_demo-0.1.0.jar .

    EXPOSE 59001

    CMD java -cp basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001



<span style="color:blue">
5.</span>


Para que seja possivel publicar no DockerHub será necessário criar credênciais:

No Dashboard **(1)** selecionar a opção _manage Jenkins_ **(2)** e depois _manage credentials_ **(3).**

![](Images/img_5.png)

No domain clicar em (global) e depois  adding some credentials em seguida definir:
* username
* password
* ID

![](Images/img_6.png)


<span style="color:blue">
6.</span>

Abrir página principal do jenkins e selecionar New item:

**(1)** Escolher um nome para o pipeline;

**(2)** Definir que pretendemos criar um novo pipeline;

![](Images/img_8.png)

**Seguir os steps infra:**

- [x] Selecionar Pipeline script from SCM;
- [x] Selecionar Git as SCM;
- [x]  URL para repositório: https://bitbucket.org/cris_dani/devops-21-22-lmn-1211756;
- [x] Especificar o branch: */master;
- [x] Especificar o path para o script:CA5/part2/react-and-spring-data-rest-basic/Jenkinsfile;

![](Images/img_10.png)


<span style="color:blue">
7.</span>

* Garantir que está atualizado o gitignore para deixar enviar o respectivo ficheiro;
* Adicionar o wrapper no repositório para o jenkins usar .\gradle\wrapper\gradle-wrapper.jar;


Enviar informação para o repositório para que possa ser processado o ficheiro **Jenkinsfile**.

    git add .
    git commit -m "Create folders: CA5 part2 (resolve #34) Clone gradle_basic_demo CA2/part2 (resolve #35) Send the Jenkinsfile to the repository (resolve #36) Send the Dockerfile to the repository (resolve #37)"
    git push

<span style="color:blue">
8.</span>

Fazer novamente o build no jenkins:

    java -jar jenkins.war

Abrir:

    http://localhost:8080/

* Selecionar o CA5_part2;
* Escolher a opção build now;

![](Images/img_14.png)

Imagem publicada no docker Hub:

    https://hub.docker.com/
![](Images/img_15.png)

<span style="color:blue">
9.</span>

Enviar informação para o repositório:

    git add .
    git commit -m "Update readme and finish CA5_part2"
    git push
    git tag ca5-part2
    git push origin ca5-part2






