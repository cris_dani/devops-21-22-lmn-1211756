## CA4 - parte 2

---

###### Containers with Docker

---

###### Docker 


<span style="color:blue">
1.</span> 

Abrir o repositório do professor e copiar o **docker-compose** para o nosso repositório para **CA4 part2**:

https://bitbucket.org/atb/docker-compose-spring-tut-demo/src/master/

Garantir que o  ficheiro tem a extensão de **yml**.

![](images/img_2.png)

<span style="color:blue">
2.</span> 

Fazer alterações ao ficheiro para que a aplicação funcione.

* Neste caso basta apenas mudar o IP:
  * web: ipv4_address: 192.168.56.10
  * db: ipv4_address: 192.168.56.11
  * networks: subnet: 192.168.56.0/24


![](images/img_3.png)

<span style="color:blue">
3.</span> 

Criar diretório para **db** e **web**. Dentro de cada pasta colocar um documento **Dockerfile** (sem extensão de ficheiro).

![](images/img_4.png)

___
###### Dockerfile - db
___

<span style="color:blue">
4.</span> 

Colocar informação conforme mencionado infra e que consta no projeto do professor Alexandre Bragança (_link do repositório no ponto 1_):

![](images/img_5.png)


___
###### Dockerfile - web
___

<span style="color:blue">
5.</span> 

Colocar informação conforme mencionado infra e que consta no projeto do professor Alexandre Bragança (_link do repositório no ponto 1_):

Aqui neste ficheiro será necessário fazer algumas alterações:

* Alterar o campo de FROM, uma vez que está a ser utilizada uma versão de **jdk11** que não é compativel com a do exemplo do professor Alexandre. Será necessário instalar o **tomcat** manualmente:
![](images/img_8.png)


* Alterar o campo do clone do repositório:

![](images/img_6.png)

* Alterar o campo de **WORKDIR** para o diretório correspondente:

![](images/img_9.png)


* O campo run ajustar para o nome da minha aplicação (validar no CA3/part2/src/settings.gradle qual o root):

![](images/img_7.png)


<span style="color:blue">
6.</span> 

Abrir o terminal e digitar os comandos infra:

    cd .\devops-21-22-lmn-1211756\
    cd ca4
    cd part2
    docker compose build

![](images/img_10.png)

<span style="color:blue">
7.</span> 

Depois de fazer o build digitar o comando infra:

    docker-compose up

<span style="color:blue">
8.</span> 

Testar o **container web** e o **container bd**:

* **webcontainer**:


  http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/

![](images/img.png)


* **dbcontainer**:

http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/h2-console
JDBC URL colocar : jdbc:h2:tcp://192.168.42.11:9092/./jpadb

![](images/img_12.png)

![](images/img_13.png)



<span style="color:blue">
9.</span> 

Fazer a tag da **web image** e enviar para o Docker Hub:


    docker login
    docker tag part2_web 1211756/part2_web:v1.0
    docker push 1211756/part2_web:v1.0

<span style="color:blue">
10.</span> 

Fazer a tag da **db image** e enviar para o Docker Hub:


    docker login
    docker tag part2_db 1211756/part2_db:v1.0
    docker push 1211756/part2_db:v1.0

<span style="color:blue">
11.</span> 

Validar que foi enviado para o **Docker Hub** as imagens:

https://hub.docker.com/

![](images/img_16.png)

<span style="color:blue">
12.</span> 

Copiar o arquivo da base de dados do contentor para a pasta na máquina host.

Vamos ter de copiar o backup: **jpadb.mv.db** para a pasta que definimos em volumes no **docker-compose.yml**.

![](images/img_15.png)


Abrir outro terminal:

    cd devops-21-22-lmn-1211756
    cd ca4
    cd part2
    docker compose exec db bash
    ls
    cp jpadb.mv.db /usr/src/data-backup

![](images/img_14.png)


<span style="color:blue">
13.</span> 

Enviar informação para o repositório

    git add . 
    git commit -m " Create folders: CA4 part2, web and db (resolve #27) Create file Dockerfile for web and db images (resolve #28) Create docker-compose.yml (resolve #29) Create readme.md (resolve #30)"
    git push
    git tag CA4-part2
    git push origin CA4-part2


___ 
## CA4 - parte 2: Alternative Solution - Kubernetes

###### O que é Kubernetes?


O **Kubernetes**, um software de código aberto desenvolvido pela plataforma Google, foi lançado em 2015. Trata-se de uma ferramenta de **orquestração** de containers que funciona **automatizando a implantação, o gerenciamento e outros processos para vários aplicativos** em containers em nuvens públicas, privadas ou híbridas e em ambientes físicos ou máquinas virtuais. 


###### Linguagem Kubernetes

- [ ]  **Master:** a máquina que controla os nós do Kubernetes. É nela que todas as atribuições de tarefas se originam.

- [ ] **Nó:** são máquinas que realizam as tarefas solicitadas e atribuídas. A máquina mestre do Kubernetes controla os nós.

- [ ] **Pod:** um grupo de um ou mais containers implantados em um único nó. Todos os containers em um pod compartilham o mesmo endereço IP, IPC, nome do host e outros recursos. Os pods separam a rede e o armazenamento do container subjacente. Isso facilita a movimentação dos containers pelo cluster.

- [ ] **Controlador de replicações:** controla quantas cópias idênticas de um pod devem ser executadas em um determinado local do cluster.

- [ ] **Serviço:** desacopla as definições de trabalho dos pods. Os proxies de serviço do Kubernetes automaticamente levam as solicitações de serviço para o pod correto, independentemente do local do pod no cluster ou se foi substituído.

- [ ] **Kubelet:** um serviço executado nos nós que lê os manifestos do container e garante que os containers definidos foram iniciados e estão em execução.

- [ ] **kubectl:** a ferramenta de configuração da linha de comando do Kubernetes.

___
* **Prós:** 

- [x] Escalabilidade horizontal automática iniciando comandos.
- [x] Rollouts e rollbacks automáticos efetuando e gerenciando configurações e mudanças em aplicativos conteinerizados.
- [x] Monitorando a eficiência dos aplicativos e reiniciando, substituindo ou destruindo aplicativos que não funcionam.
- [x] Gerenciamento de consumo de recursos e balanceamento de carga em toda a rede para manter um ambiente estável para desenvolver, testar, implantar containers de aplicativos e mantê-los em execução.
- [x] Agendamento automático


* **Contras:**

- [x] Requer muito estudo da ferramenta, o deployment para cluster é complexo.
- [x] Migração de sateless complicada.
- [x] Há incompatibilidade com o docker tools.
- [x] Utilização elevada de recursos.


___
###### O que é Docker?

O **Docker** é uma ferramenta de software DevOps de código aberto que também opera sob o conceito de conteinerização de aplicativos. No entanto, ao contrário do Kubernetes, que é uma ferramenta de orquestração, o Docker é puramente uma ferramenta de conteinerização. O Docker é usado para o desenvolvimento e implantação automatizados de containers de aplicativos. Como também é uma tecnologia de código aberto, executa aplicativos conteinerizados em várias plataformas.

Os aplicativos de container do Docker são executados isoladamente sem interferir uns com os outros, facilitando o desenvolvimento e a implantação de containers individuais. Também é relativamente fácil de configurar.

* **Prós:**

- [x] Rápido.
- [x] Leve.
- [x] Fácil de manter as versões de contentores.
- [x] Existe diversa documentação de apoio. 

* **Contras:**
- [x] Dificil de escalar sem utilizar orquestração.
- [x] Adiciona complexidade pelo desacoplamento.
- [x] Sem armazenamento.

###### Docker x Kubernetes

**O Kubernetes trabalha em conjunto com o Docker.**

Kubernetes usa o docker para criar os containers no nosso ambiente. Só que o **Kubernetes** não é utilizado apenas com esse propósito, ele é utilizado para **orquestrar e gerenciar clusters de containers**. Clusters de containers são grupos de servidores que executam a engine do Docker.

O **Docker** também tem o seu próprio orquestrador que é chamado de **Docker Swarm**, porém com o Kubernetes possuímos muito mais poder de gerenciamento, orquestração e monitoramento com os recursos que estão á nossa disposição.

O **Kubernetes** oferece dimensionamento e armazenamento automáticos. Embora seja possível aumentar ou diminuir a escala do Docker com o Docker, o processo é bastante complicado. Novamente, o Docker não oferece armazenamento automático e não possui monitoramento automático de desempenho do sistema.

![](images/img_17.png)


###### Concluindo

Não há realmente uma separação entre Kubernetes e Docker, pois cada uma dessas tecnologias foi desenvolvida com um propósito único. Na verdade, eles precisam um do outro para operar com eficiência. O Docker pode ser executado sozinho, em pequena escala, sem a necessidade de um sistema de orquestração. O Kubernetes, por outro lado, precisa de um sistema de conteinerização, Docker ou outros, com vários aplicativos conteinerizados para rodar.

| Docker                                                                                                                                                                                                                                   | Kubernetes                                                                                                                                                                                                                                                                                        |
|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| O Docker é melhor quando um utilizador tem um aplicativo complexo que requer todo o encapsulamento de um pacote e configuração em um contentor portátil.                                                                                 | O Kubernetes é bom quando é necessário ter certeza de que o aplicativo está a rodar como deveria. Caso algum contwntor não responda ou falhe, ele deve se "autocurar" e, assim, iniciar um novo contentor                                                                                         |
| Pode ser usado para qualquer um destes casos:<br/> * Se um aplicativo for adequado para contentores <br/> * Se o aplicativo não exigir nenhuma ou muita interface gráfica e se o aplicativo precisar ser implantado de forma consistente | Pode ser usado para o caso abaixo mencionado: <br/> * Quando uma organização não está comprometida com um único provedor de nuvem, usar o Kube é a escolha mais inteligente. A razão é que ele funciona da mesma forma em todos os sistemas. É por isso que é chamado de fornecedor independente. |


O uso do Kubernetes com o Docker resulta nos seguintes benefícios:

- [x] Aumenta a robustez de sua infraestrutura. Os seus aplicativos estão mais disponíveis;
- [x] Melhora a escalabilidade dos seus aplicativos.Pode-se facilmente ativar os aplicativos para lidar com mais carga sob demanda, reduzindo potencialmente o desperdício de recursos e aprimorando a experiência do utilizador;
- [x] Como os aplicativos são divididos em componentes menores, eles são mais fáceis de manter.
