## CA4 - parte 1

---

###### Containers with Docker

---

###### Docker 
Docker não é um sistema de virtualização tradicional. Enquanto que, num ambiente de **virtualização** tradicional temos um SO completo e isolado, dentro do **Docker** temos recursos isolados que, utilizando bibliotecas de kernel em comum (entre host e container), isso é possível pois o Docker utiliza como backend o nosso conhecido LXC.

O **Docker** possibilita o empacotamento de uma aplicação ou ambiente inteiro dentro de um container, e a partir desse momento o ambiente inteiro torna-se portável para qualquer outro Host que contenha o Docker instalado.

Isso reduz drasticamente o tempo de deploy de alguma infraestrutura ou até mesmo aplicação, pois não há necessidade de ajustes de ambiente para o correto funcionamento do serviço, o ambiente é sempre o mesmo, configura-se uma vez e replica-se quantas vezes for necessário.

Outra facilidade do **Docker** é poder criar imagens (containers prontos para deploy) a partir de arquivos de definição chamados **Dockerfiles**.

###### Dockerfile

**Dockerfile**, nada mais é do que um arquivo de definição onde é possível realizar ou preparar todo ambiente a partir de um script de execução. 

Através do comando **docker build**, o Docker realiza a execução desses passos e no final da execução ele encapsula cada camada gerada para dentro da imagem. 

O **Dockerfile** deve seguir uma ordem ou formatação correta para que o build seja feito de forma certa:

_O formato do texto deve respeitar:_ 
* **INSTRUÇÃO argumento**
  * Onde INSTRUÇÃO é o comando que o build deve executar, e argumento são as instruções que deve fato serão feitas;
  * A instrução deve ser escrita em maiúsculas e o argumento em minúsculas;

![](images/img_9.png)

---
#### Part_A - Build do chat server "dentro" do Dockerfile

---

<span style="color:blue">
1.</span> 

Instalar o **Docker Desktop**:

    https://www.docker.com/products/docker-desktop/

![img.png](img.png)

<span style="color:blue">
2.</span> 

Instalar **WSL 2** para versões mais antigas de WSL:

Seguir os steps infra:

    https://docs.microsoft.com/en-us/windows/wsl/install-manual#step-4---download-the-linux-kernel-update-package

<span style="color:blue">
3.</span> 

Criar uma conta no **Docker Hub**:

    https://hub.docker.com/
![](images/img_7.png)

Fazer login na aplicação:
![](images/img_5.png)

<span style="color:blue">
4.</span> 

Criar um ficheiro com o nome de **Dockerfile** e guardar dentro da pasta CA4/part1/partA. 
Deve ser um ficheiro de texto mas ter em atenção que não deve ter extensão do ficheiro.

_Exemplo: **Dockerfile.txt** deve ficar como **Dockerfile**_

![](images/img_6.png)

Abrir o documento e começar a editar a informação conforme indicado abaixo:


Este ficheiro contém as regras que vão ser interpretadas.

Por convenção:
* **Instruções** - são escritas com letras maiúsculas;
* **Argumentos** - são escritas com letras minúsculas;

![](images/img_8.png)


<span style="color:blue">
5.</span> 

Criar imagem:

    cd .\devops-21-22-lmn-1211756\
    cd .\CA4\
    cd .\part1\
    cd part_A
    docker build -t ca4part1_a .

Para validar a existência de imagens:

    docker images 

![img.png](images/i88mg.png)


<span style="color:blue">
6.</span> 

Criar e lançar o servidor:

    docker run --name container_1 -p 59001:59001 -d ca4part1_a


<span style="color:blue">
7.</span> 

Executar o chat client na máquina host:

* Abrir outra linha de comandos (**_Command Prompt_**) :

_Nota: Para validar o que está a correr naquele momento:_
    
    docker ps -a

![](images/img_2.png)

Fazer clone da aplicação:

    git clone https://Cris_Dani@bitbucket.org/luisnogueira/gradle_basic_demo.git
    gradlew build
    gradlew runClient

Vai abrir uma janela de cliente conforme apresentado na imagem infra.

![](images/img_3.png)

<span style="color:blue">
8.</span> 

Fazer tag da imagem e publicar no Docker Hub:

    docker login
    docker tag ca4part1_a 1211756/ca4part1_a:v1.0
    docker push 1211756/ca4part1_a:v1.0


<span style="color:blue">
8.</span> 

Envio da informação para o repositório:

    git add .
    git commit -m "create and update readme and commit dockerfile (resolve #22) (resolve #23) (resolve #24)"
    git push
    git tag CA4-part1_A
    git push origin CA4-part1_A


---

#### Part_B - Buil do chat no computador hospedeiro e cópia do jar para "dentro" do Docerfile

---

<span style="color:blue">
1.</span> 

Clone do gradle_basic_demo:

    git clone https://Cris_Dani@bitbucket.org/luisnogueira/gradle_basic_demo.git

<span style="color:blue">
2.</span> 

Fazer o run da aplicação:

    ./gradlew clean build

Foi criado um ficheiro jar dentro da pasta libs. Esse ficheiro deve ser colocado dentro da pasta CA4/part1/part_B.

<span style="color:blue">
3.</span> 

Criar um ficheiro com o nome de **Dockerfile** e guardar dentro da pasta CA4/part1/partB.
Deve ser um ficheiro de texto mas ter em atenção que não deve ter extensão do ficheiro.

_Exemplo: **Dockerfile.txt** deve ficar como **Dockerfile**_


![](images/img_10.png)

Abrir o documento e começar a editar a informação conforme indicado abaixo:


![](images/img_11.png)


<span style="color:blue">
4.</span> 

Para criar uma imagem:

    docker build -t ca4part1_b .

<span style="color:blue">
4.</span> 

Criar e arrancar um container:

   
      docker run --name container_2 -p 59002:59001 -d ca4part1_b

<span style="color:blue">
5.</span> 

Fazer tag da imagem e publicar no Docker Hub:

    docker login
    docker tag ca4part1_b 1211756/ca4part1_b:v1.0
    docker push 1211756/ca4part1_b:v1.0

<span style="color:blue">
6.</span> 

Executar o chat client na máquina host:

* Abrir outra linha de comandos (**_Command Prompt_**) :


    gradlew runClient

<span style="color:blue">
7.</span> 

Envio da informação para o repositório:

    git add .
    git commit -m "create and update readme and commit dockerfile (resolve #25) (resolve #26)"
    git push
    git tag CA4-part1_B
    git push origin CA4-part1_B


<span style="color:blue">
8.</span> 

Validar que a informação foi remetida para o Docker Hub:

    https://hub.docker.com/repositories

![](images/img_12.png)