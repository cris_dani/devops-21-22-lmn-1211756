## CA3 - parte 1

---

###### Virtualização com Vagrant

---

O termo **virtualização** tem origem no conceito “_virtual_”, ou seja, algo abstrato que simula as características de algo real.
Esses ambientes virtuais são isolados e gerenciados por um software de **Hypervisor** que fornece e controla os recursos da máquina com o objetivo de permitir a execução concorrente de vários sistemas operacionais.


**Por exemplo**: um utilizador tem o Windows instalado no computador, mas deseja utilizar um software que está disponível apenas para o Linux, graças à virtualização, esse utilizador pode executar uma versão de qualquer sistema operacional (incluindo os seus aplicativos) no seu próprio computador, sem necessariamente ter que o instalar fisicamente.

![](Image/img_14.png)

As máquinas virtuais simulam máquinas físicas com seu próprio sistema operacional, kernel, e aplicações, o que as tornam mais flexíveis quanto a finalidade de sua utilização, porém, demandam mais tempo de inicialização e poder de processamento do hospedeiro uma vez que a memoria e outros recursos precisam ser previamente alocados para seu correto funcionamento.


### Como a virtualização funciona?

Uma solução de virtualização é composta, essencialmente, por dois "protagonistas": o **hospedeiro (host)** e o **hóspede ou convidado (guest)**. Podemos entender o hospedeiro como sendo o sistema operacional que é executado por uma máquina física. O hóspede, por sua vez, é o sistema virtualizado que deve ser executado pelo hospedeiro. A virtualização ocorre quando estes dois elementos existem.

### Parte prática:

### Instalar o VirtualBox

<span style="color:blue">
1.</span> 

Instalar a **VirtualBox** seguindo o link:

    https://www.virtualbox.org/

![](Image/img.png)

<span style="color:blue">
2.</span> 

Abrir o **VirtualBox** e criar uma máquina virtual clicando em new:

![](Image/img_1.png)

<span style="color:blue">
3.</span>  

Preencher os campos infra conforme imagem:

**Machine Folder** - Corresponde ao local onde serão armazenados os ficheiros, pode ser mantida a localização sugerida;

![](Image/img_2.png)

<span style="color:blue">
4.</span>  

**Memory Size** - selecionar a opção de 2048MB de RAM.
_**nota**: tentar manter sempre a memória a utilizar no intervalo verde;_

![](Image/img_3.png)

<span style="color:blue">
5.</span> 

**Hard disk** - manter a opção dos 10,00GB e manter a seleção de "_create a virtual hard disk now_".
_**nota** - este valor de 10,00GB corresponde ao limite de espaço a utilizar e não vai logo reservar a totalidade deste valor no disco_;

<span style="color:blue">
6.</span> 

**Hard disk file type** - selecionar **VDI** (_virtualBox Disk Image_);

<span style="color:blue">
7.</span> 

**Storage on physical hard disk** - selecionar _Dynamically allocated_;
_**nota** - Só vai usar até 10,00GB no máximo, à medida da necessidade;_

<span style="color:blue">
8.</span> 

**File location and size** - manter a localização apresentada;

<span style="color:blue">
9.</span> 

No final selecionar _create_ e irá surgir a máquina criada conforme exemplo infra:
![](Image/img_4.png)


### Instalar o Ubuntu

<span style="color:blue">
1.</span> 

Seguir o link para download do **minimal iso** (imagem de um cd rom). Escolher a versão de **64-bit**:

    https://help.ubuntu.com/community/Installation/MinimalCD

![](Image/img_5.png)

<span style="color:blue">
2.</span>

Abrir o **virtualBox** e, na VM criada anteriormente aceder aos settings:
![](Image/img_6.png)

<span style="color:blue">
3.</span>

Abrir a planilha de **storage**, e em **storage devices** carregar no **empty** e depois no círculo e selecionar o **mini.iso** (_download realizado no step 1_) e ativar a opção **Live CD/DVD** conforme demonstra figura infra:
![](Image/img_7.png)

<span style="color:blue">
4.</span>

Arrancar novamente a virtualBox clicando na seta verde de start presente no lado direito superior;

<span style="color:blue">
5.</span>

Surge um ecrã de instalação do **ubuntu**:
* Selecionar **install**;
* Selecionar a língua de Portugal;
* **Configurar o teclado** – selecionar opção sim e seguir os steps indicados, no final aparece uma mensagem indicando que foi detetado um teclado em PT, confirmar;
* Preencher o **nome do computador**: pode-se deixar como ubuntu;
* **Mirror do arquivo** ubuntu deixar a opção de Portugal selecionada;
* Confirmar o arquivo do mirror pt.archive.ubuntu.com;
* **Proxy HTTP** não colocar nada;
* Definir utilizadores e password;
* Configurar o relógio;
* **Particionar discos** – selecionar a opção **Guiado** – utilizar o disco inteiro (primeira opção) e confirmar que é esse mesmo o disco a usar;
* **Configuração PAM** – questiona se é para fazer atualizações automáticas, devemos selecionar a opção de “_sem atualizações automáticas_”;
* **Seleção de software** – não selecionar nada e avançar na instalação;
* Instalar o **GRUB** no disco rígido – selecionar a opção sim;
* **UTC** para fuso horário – selecionar sim;
* Instalação concluída;

<span style="color:blue">
6.</span>

Remover o **mini.iso** indo a **devices** – **optical drives** – **remove disk** from virtual drive:

![](Image/img_8.png)


<span style="color:blue">
7.</span>

Finda a instalação será necessário fazer o **login** na máquina;


### Configurar Host-only network

<span style="color:blue">
1.</span>

    File -> Host Network Manager (VirtualBox Host-Only Ethernet Adapter);

<span style="color:blue">
2.</span>

Desligar o computador para conseguir ligar o _adapter 2_; 

<span style="color:blue">
3.</span>

Aceder aos **settings** – **network** – **adapter 2** – ativar a opção **enable network adapter** e- em **Attached** to escolher a opção **Host-only Adapter** e, o nome da rede (_VirtualBox Host-Only Ethernet Adapter_);

![](Image/img_9.png)


### Instalação de ferramentas

<span style="color:blue">
1.</span>

Para atualizar todos os pacotes de repositórios:

    Sudo apt update

<span style="color:blue">
2.</span>

Para fazer a gestão da rede:

    sudo apt install net-tools

<span style="color:blue">
3.</span>

Permitir configurar a rede atribuindo um IP:

    sudo nano /etc/netplan/01-netcfg.yaml

<span style="color:blue">
4.</span>

Executar o comando de rede abre um **ficheiro nano**, será necessário garantir que toda a informação do texto infra consta nesse documento:
* **Ctrl O** para gravar as alterações;
* **Ctrl X** para sair do nano;


    network:
    version: 2
    renderer: networkd
    ethernets:
    enp0s3:
    dhcp4: yes
    enp0s8:
    addresses:
     - 192.168.56.1/24

<span style="color:blue">
5.</span>

Aplicar as alterações:

    sudo netplan apply

<span style="color:blue">
6.</span>

Aceder remotamente a esta máquina:

    sudo apt install openssh-server

<span style="color:blue">
7.</span>

Autenticação por introdução de password:

    sudo nano /etc/ssh/sshd_config 

Procurar a linha com informação de **PasswordAuthentication yes** (comentada com # e remover este #);
* **Ctrl O** para gravar;
* **Ctrl X** para sair;


<span style="color:blue">
8.</span>

Para ler a alteração:

    sudo service ssh restart

<span style="color:blue">
9.</span>
 
Copiar ficheiros remotamente:

    sudo apt install vsftpd

<span style="color:blue">
10.</span>

Editar o ficheiro para permitir a escrita:

    sudo nano /etc/vsftpd.conf 

Procurar a linha com informação de  write_enable=YES (comentada com # e remover este #);
* **Ctrl O** para gravar;
* **Ctrl X** para sair;



### Aceder remotamente e instalar fileZilla (partilha arquivos)

<span style="color:blue">
1.</span>

Abrir a linha de comandos **<span style="color:limegreen">**PowerShell**</span>** e digitar:

    ssh user@IP (ssh cristiana@192.168.56.1)

<span style="color:blue">
2.</span>

Instalar o **<span style="color:limegreen">**FileZilla**</span>** seguindo o link: 

    https://filezilla-project.org/

**<span style="color:orange">**O FileZilla é um aplicativo de código aberto, para Windows, Mac e Linux, recomendado para quem precisa enviar arquivos para algum servidor através do protocolo FTP (File Transfer Protocol).**</span>**
* **No host** – colocar o IP;
* **Username** – colocar o user name definido;

![](Image/img_10.png)

### Instalar pré requisitos para correr Spring

<span style="color:blue">
1.</span>

Instalar o **git**:

    sudo apt install git

<span style="color:blue">
2.</span>

Instalar o **java**:

    sudo apt install openjdk-11-jdk-headless

Para validar a versão jdk instalada:

    java -version

<span style="color:blue">
3.</span>

Fazer clone da aplicação:

    git clone https://github.com/spring-guides/tut-react-and-spring-data-rest.git

<span style="color:blue">
4.</span>

Entrar na pasta com o seguinte comando:

    cd tut-react-and-spring-data-rest/

<span style="color:blue">
5.</span>

Entrar na pasta basic **cd basic**;

<span style="color:blue">
6.</span>

Correr o boot run: 

    ./mvnw spring-boot:run

<span style="color:blue">
7.</span>

Abrir a aplicação:

    http://192.168.56.1:8080/

### Clone do repositório local

<span style="color:blue">
1.</span>

Fazer clone da do repositório local:

    git clone git@bitbucket.org:cris_dani/devops-21-22-lmn-1211756.git



### Build de um projeto Maven

![](Image/img_11.png)

<span style="color:blue">
1.</span>


Instalar a dependência de **Maven**:

    sudo apt install maven

* Quando solicitado, digite **S** e pressione **Enter** para confirmar a instalação;


  Para validar a versão maven instalada:

    mvn -version


<span style="color:blue">
2.</span>

Entrar na pasta de devops:

    cd devops-21-22-lmn-1211756/

<span style="color:blue">
3.</span>

Entrar na pasta de CA1:

    cd CA1/
    cd part/1

<span style="color:blue">
4.</span>

Entrar na pasta do tut react:

    cd tut-react-and-spring-data-rest/

<span style="color:blue">
5.</span>

Entrar na pasta basic:

    cd basic/

<span style="color:blue">
6.</span>

Fazer o build da aplicação (**spring boot**):

    sudo ./mvnw spring-boot:run

<span style="color:blue">
7.</span>

Abrir o link infra:

    http://192.168.56.1:8080/

![](Image/img_12.png)



### Build de um projeto Gradle

![](Image/img_13.png)

<span style="color:blue">
1.</span>

Instalar a dependência de **Gradle**:

* Abrir a pasta de tmp:


    cd /tmp

* Download do gradle:


    wget https://services.gradle.org/distributions/gradle-7.4.2-bin.zip

<span style="color:blue">
2.</span>

Instalar o unzip: 

    sudo apt install unzip


Extrair a configuração binária gradle no diretório /opt/gradle:

    
    unzip gradle-*.zip


<span style="color:blue">
3.</span>

Uma vez feito, copie o conteúdo para o diretório /opt/Gradle usando o comando cp no Ubuntu. Para isso, digite o seguinte comando no terminal:

    sudo mkdir /opt/gradle
    sudo cp -pr gradle-*/* /opt/gradle

_**nota** - observe que esses comandos acima serão executados apenas no diretório tmp. Altere o local depois de copiar o pacote._

<span style="color:blue">
4.</span>

Definir as variáveis de ambiente:

É necessário adicionar os caminhos para o Gradle. Para tal será criado um caminho chamado **gradle.sh** no diretório /etc/profile.d/ conforme é mostrado infra:

    sudo vi /etc/profile.d/gradle.sh

<span style="color:blue">
5.</span>

Copiar o seguinte comando:

    export PATH=/opt/gradle/bin:${PATH}

<span style="color:blue">
6.</span>


O próximo passo é **alterar as permissões** usando o comando **chmod**. O comando chmod altera o modo de acesso do arquivo, ou seja, para permitir que ele leia ou escreva. Para isso, digite o seguinte comando no terminal:

    sudo chmod +x /etc/profile.d/gradle.sh

<span style="color:blue">
7.</span>

Agora é preciso inicializar as variáveis de ambiente que definimos para o **Gradle** no ambiente atual. Isso pode ser feito usando o comando **source** conforme mostrado abaixo:


    source /etc/profile.d/gradle.sh

<span style="color:blue">
8.</span>

Entrar na pasta de devops:

    cd devops-21-22-lmn-1211756/

<span style="color:blue">
9.</span>

Entrar na pasta de CA2:

    cd CA2/
    cd part2

<span style="color:blue">
10.</span>

Entrar na pasta do tut react:

    cd react-and-spring-data-rest/

<span style="color:blue">
11.</span>

Entrar na pasta basic:

    cd basic/

<span style="color:blue">
12.</span>

Fazer o build da aplicação (**spring boot**):

    gradle build
    ./gradlew build 

<span style="color:blue">
13.</span>


**Execute o servidor**

Abrir a **VM** e executar o seguinte comando no diretório raiz do projeto:

    java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001


![](Image/img_15.png)

<span style="color:blue">
14.</span>


**Executar um cliente**

Abrir outro terminal e execute a seguinte tarefa gradle no diretório raiz do projeto:

    ./gradlew runClient

![](Image/img_16.png)

![](Image/img_17.png)

______________________________________________________________________

### CA3-Part 1 - Objetivos/Requisitos - _Considerações_


* **A VM ubuntu server não tem ambiente gráfico**, como tal, aplicações sem ambiente gráfico podem ser executadas no servidor como é o caso do chat server, mas o aplicativo "cliente" de chat requer ambiente de gráfico nao sendo possivel correr no servidor ubuntu, terá de correr no **host**.


* Outro motivo em termos práticos está relacionado com a necessidade de ter vários clientes chat ligados, se tivermos o servidor a correr num host separado dos clientes, **cada cliente pode sair ou entrar na aplicação sem causar impacto para os restantes**.


* Ao executar o Gradle no Windows vs. Ubuntu, pode ser necessário ajustar as opções de **commandLine**. Isto implica ser necessário escrever **um novo** tipo de **tarefa (task)** que permita **especificar cada sistema operacional** e, essa tarefa será responsável por escolher a linha de comando correta de acordo com o sistema operacional a que se refere.


* Pode também haver **incompatibilidade dentro das próprias versões de Gradle**, por exemplo uma versão instalada utiliza o código _archiveFileName_ (**versão mais recente**) enquanto que, para versões mais antigas o _archiveName_ é suficiente.

Exemplo para a task zip:
![](Image/img_18.png)