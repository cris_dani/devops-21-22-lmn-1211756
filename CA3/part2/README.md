## CA3 - parte 2

---

###### Virtualização com Vagrant

---

**Vagrant, o que é?**

O Vagrant é um gerenciador de VMs (máquinas virtuais). Através dele é possível definir o ambiente de desenvolvimento onde o projeto irá rodar. Com suporte para Mac OSX, Linux e Windows, consegue atender boa parte dos utilizadores. Utiliza **providers**, **boxes** e se necessário os **provisioners.**

**Vantagem?**

**A grande vantagem deste software é que possui um arquivo de configuração chamado Vagrantfile** onde está centralizada toda a configuração da VM que queremos criar. Podemos usar o Vagrantfile para criar uma VM exatamente o mesmo quantas vezes quisermos. Também é super leve, então podemos adicioná-lo ao nosso repositório ou enviar por e-mail para colegas de trabalho.

- **Providers** - 
O Vagrant precisa de ter um provider instalado. É aqui que a máquina virtual, contendo o seu ambiente, irá rodar. O Vagrant dá suporte a providers como VirtualBox, VMware e AWS.

- **Boxes** - 
Com o objetivo de evitar o processo moroso de criar uma VM do zero, o Vagrant trabalha com **boxes**. Elas são imagens base para ele clonar uma VM. Definir uma box para o ambiente **é o primeiro passo a ser feito na configuração do Vagrantfile.** Caso a box selecionada já tenha tudo o que é preciso, não será necessário haver preocuupação com os provisioners.

- **Provisioners** - 
Com provisioners é possível pré-instalar aplicações, definir configurações e realizar toda a parte de ajuste fino de uma box para atender a todas as  necessidades. É possível utilizar desde shell scripts básicos até sistemas de gerenciamento de configurações como o Chef, Puppet e Docker.


## Instalar o Vagrant

<span style="color:blue">
1.</span> 

Abrir o link do Vagrant que se encontra infra:

    https://www.vagrantup.com/downloads.html

Escolher sistema operativo (amd 64 para windows):


![](Image/img.png)

<span style="color:blue">
2.</span>

Validar que a versão é a **2.2.19**;

    vagrant -v

<span style="color:blue">
3.</span>

Criar um projeto Vagrant é tão simples quanto definir o diretório raiz do projeto e definir um arquivo Vagrant.

    mkdir my-first-vagrant
    cd my-first-vagrant

<span style="color:blue">
4.</span>

Podemos utilizar o comando **vagrant init <box>** para que seja criado o arquivo do Vagrantfile:

    vagrant init envimation/ubuntu-xenial

Aqui escolhemos escolher a box de **ubuntu 16.04** mas, seguindo o link infra podemos escolher outras imagens.

https://app.vagrantup.com/boxes/search

![](Image/img_1.png)

###### VagrantFile

O arquivo **Vagrantfile** é o arquivo de configuração do Vagrant para o seu projeto e é baseado em linguagem _Ruby_. É nele que definimos tudo da sua máquina virtual, versão do OS, memória, IP de rede entre entre outros.

Ao executar o comando **vagrant init <box>** um arquivo **Vagrantfile** é criado com diversas opções já preenchidas, porém comentadas. Podemos passar o parâmetro **-m** ou **--minimal** para remover todas as linhas comentadas e deixar somente o necessário.

    Vagrant.configure("2") do |config|
    config.vm.box = "envimation/ubuntu-xenial"
    end


<span style="color:blue">
5.</span>

A box representa o computador que está a ser criado virtualmente. Entre **""** surge o nome da máquina **Vagrant**. 

    config.vm.box = "envimation/ubuntu-xenial"

<span style="color:blue">
6.</span>

Inicializa a máquina virtual e executa o **provisioner**.

    vagrant up


###### Provisioner

Um provisionador / orquestrador é o software que irá automatizar as tarefas que serão executadas no ambiente virtual assim que o sistema operacional estiver disponível.

<span style="color:blue">
7.</span>

Para acessar a máquina virtual via **ssh**:

    vagrant ssh
    vagrant halt (para desligar a VM)

<span style="color:blue">
8.</span>

Para editar o vagrant file usar o git bash

    nano Vagrantfile

<span style="color:blue">
9.</span>

Instalar o Apache (provision section);

Descomentar as linhas do vagrant file de **"config.vm-provision"** em diante até **"SHELL"**.

<span style="color:blue">
10.</span>

Atribuir uma porta da VM para a máquina host:

Descomentar as linhas **"config.vm.network" "forwarded-port"**.

Mudar o host:8080 para host:8010

<span style="color:blue">
11.</span>

Configurar um IP estático privado na VM:
Descomentar as linhas **"config.vm.network" "private_network".**
Mudar o IP: 192.168.33.10" para IP: 192.168.56.5


<span style="color:blue">
12.</span>

Configurar uma pasta compartilhada para as páginas do servidor web
Descomentar as linhas **"config.vm.synced_folder"**.
Mudar **"../data", "/vagrant_data"** para **"./html", "/var/www/html"**.

<span style="color:blue">
13.</span>

    vagrant reload --provision



## Editar o vagrant file para criação de duas máquinas virtuais


<span style="color:blue">
1.</span>

Clonar o repositório de **CA2 parte 2** para o repositório de **CA3 parte 2**.

<span style="color:blue">
2.</span>

Guardar uma cópia do **vagrantfile** disponivel no link infra também na pasta de **CA3 parte 2**.

https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/src/master/



###### Configurar a primeira máquina virtual **(db)**:
_____

<span style="color:blue">
1.</span>

**Abrir o ficheiro de vagrantfile.**

Será necessário alterar a máquina virtual a utilizar para que a mesma seja compativel com o **jdk 11**.

**(ubuntu/bionic64)**

![](Image/img_7.png)

<span style="color:blue">
2.</span>

No **provision** será necessário alterar a versão de **jdk**, igualmente para a versão 11:

![](Image/img_3.png)

<span style="color:blue">
3.</span>

Nas configurações da database alterar novamente a máquina virtual para a mesma mencionada no **ponto 1**.

**(ubuntu/bionic64)**

![](Image/img_4.png)



###### Configurar a segunda máquina virtual **(webServer)**:
_____

<span style="color:blue">
1.</span>

Será também necessário fazer alterações ao nível do **webserver**.

![](Image/img_5.png)

<span style="color:blue">
2.</span>

Alterar os comandos para clonar o repositório conforme mencionado infra:

    git clone https://Cris_Dani@bitbucket.org/cris_dani/devops-21-22-lmn-1211756.git
    cd devops-21-22-lmn-1211756/CA2/part2/react-and-spring-data-rest-basic
    chmod u+x gradlew
    ./gradlew clean build

Atualizar o directório que tinha o ficheiro war e o nome do respectivo ficheiro:

    sudo cp ./build/libs/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT.war /var/lib/tomcat8/webapps

![](Image/img_6.png)

###### Alteração do application.properties

<span style="color:blue">
1.</span>


ir a _src - resources - application.properties_ adicionar a informação infra:


![](Image/img_11.png)

    server.servlet.context-path=/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT
    spring.data.rest.base-path=/api
    spring.datasource.url=jdbc:h2:tcp://192.168.56.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
    spring.datasource.driverClassName=org.h2.Driver
    spring.datasource.username=sa
    spring.datasource.password=
    spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
    spring.jpa.hibernate.ddl-auto=update
    spring.h2.console.enabled=true
    spring.h2.console.path=/h2-console
    spring.h2.console.settings.web-allow-others=true

<span style="color:blue">
2.</span>

###### Alteração do build.gradle

No **build.gradle** copiar a informação infra e colar:

    plugins {
    id 'org.springframework.boot' version '2.6.6'
    id 'io.spring.dependency-management' version '1.0.11.RELEASE'
    id 'java'
    id "org.siouan.frontend-jdk11" version "6.0.0"
    }

    group = 'com.greglturnquist'
    version = '0.0.1-SNAPSHOT'
    sourceCompatibility = '11'

    repositories {
    mavenCentral()
    }

    dependencies {
    implementation 'org.springframework.boot:spring-boot-starter-data-jpa'
    implementation 'org.springframework.boot:spring-boot-starter-data-rest'
    implementation 'org.springframework.boot:spring-boot-starter-thymeleaf'
    runtimeOnly 'com.h2database:h2'
    testImplementation 'org.springframework.boot:spring-boot-starter-test'

    providedRuntime 'org.springframework.boot:spring-boot-starter-tomcat'
    }

    frontend {
    nodeVersion = "14.17.3"
    assembleScript = "run webpack"
    //	cleanScript = "run clean"
    //	checkScript = "run check"
    }

<span style="color:blue">
3.</span>

###### Alteração do package.json

No **package.json** alterar a informação conforme indicado infra nos **scripts**:

Nota: alteração apenas da primeira linha e acrescentar a informação referente à segunda linha. O resto da informação apenas surge para garantir que a mantemos e nao a eliminamos.

    "scripts": {
    "watch": "webpack --watch -d",
    "webpack": "webpack",
    "build": "npm run webpack",
    "check": "echo Checking frontend",
    "clean": "echo Cleaning frontend",
    "lint": "echo Linting frontend",
    "test": "echo Testing frontend"
    },

<span style="color:blue">
4.</span>

###### Criação de uma classe java

Criar uma classe java com nome **ServletInitializer** e inserir a informação infra:

![](Image/img_8.png)

    package com.greglturnquist.payroll;

    import org.springframework.boot.builder.SpringApplicationBuilder;
    import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

    public class ServletInitializer extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(ReactAndSpringDataRestApplication.class);
    }
    }

<span style="color:blue">
5.</span>

###### Alteração da app.js

Na app.js alterar a informação que consta na linha 18 do path para a informação infra:

    '/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/api/employees'

![](Image/img_9.png)

<span style="color:blue">
6.</span>

###### Alteração do index.html

No index.html alterar a informação que consta na linha 6:

    <link rel="stylesheet" href="main.css" />


![](Image/img_10.png)

<span style="color:blue">
7.</span>

###### Alteração do gitignore

No **gitignore** colar a informação que consta no repositório infra:

https://bitbucket.org/atb/tut-basic-gradle/commits/27426c9faacc70768646baae7e351476ba550926

![](Image/img_12.png)

<span style="color:blue">
8.</span>

Fazer commit de todas as alterações:

    git add .
    git commit -m "Upgrade Vagrantfile (resolve #17) Upgrade other files (resolve #18) Create class ServletInitializer (resolve #19) Update readme (resolve #20)"
    git push

__________________________ 

###### Testar as máquinas virtuais de DB e web

<span style="color:blue">
1.</span>

    cd .\devops-21-22-lmn-1211756\
    cd .\CA3\
    cd part2

O next step consiste em construir o ambiente de acordo com o que se encontra parametrizado no Vagrantfile:

    vagrant up

![](Image/img_16.png)

![](Image/img_17.png)




- **(web VM)**  http://192.168.56.10:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/ 


![](Image/img_20.png)

- **(db VM)** http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/h2-console


![](Image/img_18.png)

![](Image/img_19.png)


# Alternativa ao VirtualBox - VMware


Ambos são excelentes sistemas de virtualização gratuitos e de várias plataformas o que pode dificultar a escolhe entre eles para criar uma máquina virtual.



![](Image/img_13.png)

O VirtualBox é executado no Windows, Linux, Mac OS e Solaris e suporta uma ampla variedade de sistemas operacionais convidados, do Windows ao Linux, Mac, Solaris, OpenSolaris, OS/2 e variações do BSD.

A página de download  mostra a lista das distribuições mais recentes, dependendo dos sistemas host.

**Para Windows e outros sistemas, a instalação é muito simples, porque é feita com os métodos específicos para cada plataforma.**


![](Image/img_14.png)

**O VMware pode ser instalado apenas no Windows e Linux.**

No entanto, ele suporta uma ampla variedade de sistemas operacionais convidados e fornece ferramentas úteis para verificar a compatibilidade .

Podem ser obtidos os executáveis para executar a instalação, dependendo do sistema, conectando-se à página de download .
Enquanto que, para o Windows, seguindo todas as etapas e instruções do assistente, a instalação será muito simples, para Linux, sendo um pacote “.bundle”, teremos que fazer a instalação como root e usar o “sh” comando.


### VBox vs VMware

![](Image/img_15.png)

### Implementação da alternativa

###### Instalação

<span style="color:blue">
1.</span>

Instalar o **VMWare** seguindo o link infra:

    https://customerconnect.vmware.com/en/downloads/details?downloadGroup=WKST-PLAYER-1623-NEW&productId=1039&rPId=85399

Seguir os steps de instalação da aplicação.

<span style="color:blue">
2.</span>

Copiar:

* A aplicação de react-and-spring-data-rest de CA3/part2 para para CA3/part2_Alternative. 
* O ficheiro **vagrantfile** de CA3/part2 para CA3/part2_Alternative.

###### Alterações no Vagrantfile

Na linha **4** alterar para box compativel com VMWare e jdk11:

    config.vm.box = "hashicorp/bionic64"

Na linha **20** onde consta a informação referente a  **Configurations specific to the database VM** alterar para:

    config.vm.box = "hashicorp/bionic64"

Na linha **22** alterar o IP da máquina **DB**:

    db.vm.network "private_network", ip: "192.168.142.11"

Na linha **49** alterar o IP da máquina **WEB**:

    web.vm.network "private_network", ip: "192.168.142.10"

Na linha **47** onde consta a informação referente a  **Configurations specific to the webserver VM** alterar para:

    config.vm.box = "hashicorp/bionic64"

Na linha **52** alterar o provider:

    web.vm.provider "vmware_desktop" do |v|

Na linha **72** alterar o diretório

    cd devops-21-22-lmn-1211756/CA3/Part2_Alternative/react-and-spring-data-rest-basic

###### Alterações no application.properties

![](Image/img_21.png)

Na linha **3** alterar a **spring.datasource.url**:

    spring.datasource.url=jdbc:h2:tcp://192.168.142.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE

###### Instalar o Vagrant VMware provider plugin

    vagrant plugin install vagrant-vmware-desktop

__________________________ 

###### Testar as máquinas virtuais de DB e web

    cd .\devops-21-22-lmn-1211756\
    cd .\CA3\
    cd Part2_Alternative

O next step consiste em construir o ambiente de acordo com o que se encontra parametrizado no Vagrantfile:

    vagrant up --provider vmware_desktop

- **(web VM)**  http://192.168.142.11:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/



- **(db VM)** http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/h2-console

